module.exports = {
  extends: ["airbnb", "prettier"],
  env: {
    browser: true,
    node: true
  },
  plugins: ["html", "prettier"]
};
