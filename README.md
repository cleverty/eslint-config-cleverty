# eslint-config-cleverty

This is the JavaScript Style Guide for Cleverty and published on [npm](https://www.npmjs.com/package/eslint-config-cleverty).

## What is it?

- [ESLint](https://eslint.org/) based on the [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript).
- [Prettier](https://prettier.io/) as code formatter.
- [eslint-config-prettier](https://github.com/prettier/eslint-config-prettier) to turn off all rules that are unnecessary or might conflict with Prettier.
- [eslint-plugin-html](https://www.npmjs.com/package/eslint-plugin-html) for linting and fixing inline scripts contained in HTML files.

## Local Install

- If you don't already have a `package.json` file, create one with `npm init`.

- Install everything needed

```
npx install-peerdeps --dev eslint-config-cleverty
```

- Create file `.eslintrc` in the root of your project's directory

```json
{
  "extends": ["cleverty"]
}
```

An alternative is to put this object in your `package.json` under the property `"eslintConfig":`.

- You can add two scripts to your package.json to lint and/or fix:

```json
  "scripts": {
    "lint": "eslint .",
    "lint:fix": "eslint . --fix"
  },
```

## Global Install

- Install everything needed

```
npx install-peerdeps --global eslint-config-wesbos
```

- Create a global `.eslintrc` file:

ESLint will look for one in your home directory

- `~/.eslintrc` for mac
- `C:\Users\username\.eslintrc` for Windows

Your `.eslintrc` file should look like this:

```json
{
  "extends": ["cleverty"]
}
```

## Visual Studio Code

- Install the [VS Code ESLint extension](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

- Change your Visual Studio Code settings via `Code/File` → `Preferences` → `Settings` or by editing the `settings.json` file (click the `{}` icon in the top right corner)

```js
// auto-save
"editor.formatOnSave": true,
// turn it off for JS and JSX, we will do this via eslint
"[javascript]": {
  "editor.formatOnSave": false
},
"[javascriptreact]": {
  "editor.formatOnSave": false
},
// ESLint plugin runs on save
"eslint.autoFixOnSave": true,
// If the Prettier extension is enabled for other languages like CSS and HTML, turn it off for JS since ESLint is already doing it
"prettier.disableLanguages": ["javascript", "javascriptreact"],
```

## Removal

For a local removal, omit the `--global` flag, remove your `package-lock.json` and delete the `node_modules` folder.

```
npm remove --global eslint-config-cleverty eslint eslint-config-airbnb eslint-config-prettier eslint-plugin-html eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-react prettier
```
